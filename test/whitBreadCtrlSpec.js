describe('whitbreadCtrl', function(){
    var scope,mockResponse,
        model,service,httpBackend,request,
        $templateCache,$compile,
        ctrl;
    beforeEach(module('whitbreadApp', 'mockResponse'));

    beforeEach(inject(function($rootScope, $controller, _venueModel_, _foursquareService_,$httpBackend,_mockResponse_,_$templateCache_, _$compile_) {
        $templateCache = _$templateCache_;
        $compile = _$compile_;
        scope = $rootScope.$new();
        httpBackend = $httpBackend;
        mockResponse = _mockResponse_;
        model = _venueModel_;
        service = _foursquareService_;
        ctrl = $controller('whitbreadCtrl', {$scope: scope});
    }));

    it('should set the scope of the controller to be empty', function() {
        expect(scope.venues).toBeFalsy();
    });

    it('should set the scope to display some results', function() {
        var query = 'kingsley';
        request = service.endpoint + query;
        httpBackend.when("GET", request).respond(mockResponse);
        httpBackend.expectGET(request);
        scope.searchForVenue(query);
        httpBackend.flush();
        expect(scope.venues).toBeTruthy();
    });
});