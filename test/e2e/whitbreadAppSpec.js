describe('Whitbread Search App', function() {
    beforeEach(function() {
        browser.get('/');
    });
    describe("Initialisation", function() {
        it('should init template view', function() {
            var searchForm = element(by.css('.search-form'));
            expect(searchForm.isPresent()).toBeTruthy();
        });

        it('should load an empty results lisk view', function() {
            var searchResultsList = element(by.repeater('venue in venues'));
            expect(searchResultsList.count()).toEqual(0);
        });
    });

    describe("Interaction", function() {
        it('should update the search results list with results when keyword is typed', function() {
            var searchResultInput = element(by.model('searchText'));
            //$(searchResultInput).val('kingsley');
            $('input.search-form').val('kingsley');
            expect(element(by.repeater('venue in venues')).count()).toBeFalsy();

        });

        it('should show no results when search is empty', function() {
            var searchResultInput = element(by.css('searchText'));
            //$(searchResultInput).val('');
            $('input.search-form').val('kingsley');
            expect(element(by.repeater('venue in venues')).count()).toBeFalsy();

        });
    })
});