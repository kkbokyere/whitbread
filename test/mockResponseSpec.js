angular.module('mockResponse',[])
    .value('mockResponse',{
      response: {
        venues: [{
          id: "4a43bcb7f964a520bba61fe3",
          name: "Brooklyn Bridge",
          contact: {
            twitter: "nyc_dot",
            facebook: "166279802886",
            facebookUsername: "NYCDOT",
            facebookName: "NYC DOT"
          },
          location: {
            address: "Brooklyn Bridge",
            lat: 40.705953265881305,
            lng: -73.99656772613525,
            distance: 723,
            postalCode: "10038",
            cc: "US",
            city: "New York",
            state: "NY",
            country: "United States",
            formattedAddress: [
              "Brooklyn Bridge",
              "New York, NY 10038",
              "United States"
            ]
          },
          categories: [
            {
              id: "4bf58dd8d48988d1df941735",
              name: "Bridge",
              pluralName: "Bridges",
              shortName: "Bridge",
              icon: {
                prefix: "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/bridge_",
                suffix: ".png"
              },
              primary: true
            }
          ],
          verified: true,
          stats: {
            checkinsCount: 151202,
            usersCount: 86488,
            tipCount: 725
          },
          url: "http://www.nyc.gov/dot",
          specials: {
            count: 0,
            items: [ ]
          },
          storeId: "",
          hereNow: {
            count: 15,
            summary: "15 people are here",
            groups: [
              {
                type: "others",
                name: "Other people here",
                count: 15,
                items: [ ]
              }
            ]
          },
          referralId: "v-1457823029",
          venueChains: [
            {
              id: "556a2f99a7c8957d73d43f24"
            }
          ]
        }]
      }
    });