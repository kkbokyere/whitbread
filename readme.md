# Whitbread Test

## Synopsis

This repository is made specifically for the Whitebread product developer test. The application is a simple AngularJS app.
The application is a simple search application, which allows users to search for a place and give back suggested results. The application is an instant search.

## Basic structure

The basic structure looks something like this:

```
.
├── css
│   ├── main.css
│   └── normalize.css
├── doc
├── test
├── views
├── js
│   ├── controllers/
│   ├── models/
|   ├── services/
│  
├── sass
│   ├── main.scss
│  
├── index.html
```

# Prerequisite

These are some of the following tools used within the project, any developer wishing to work on it, needs to ensure they have them installed.

The following setup instructions should get you on your way:

- Before you install any of these tools, please make sure you have NodeJS and NPM installed. You can find setup instructions for them here: [NodeJS].

- Make sure you run all these commands from the root directory of this project i.e. the directory where all the configuration (bower.json, package.json) files are.

- You may need to run these commands with 'sudo' (Mac Users Only). Run the following commands to get you on your way for development:

##Clone this repository using git:
```sh
git clone https://kkbokyere@bitbucket.org/kkbokyere/whitbread.git
cd whitbread
```

## Installation

##Install Dependencies
After installing Node, you now have access to the NPM cli via terminal. we will use this to install all dependencies. Other front-end dependencies is installed via 'bower'. I have pre configured npm to also run a 'bower install'. This script should get you everything you need.

##Main Installation Script
```sh
#install all requried node modules
$ npm install
```

##Run the Application

I have preconfigured the project with a simple development web server. The simplest way to start this server is:

```sh
npm start
```

Now browse to the app at http://localhost:8000/. 
You should now see the application working in full. I used a NPM module called http-server, which will allow a simple and quick local server for the project.

## Testing
I have written two kinds of test for testing this application. Unit tests and end to end tests.

###Unit Testing (Karma)
All unit test run through Karma test runner and using Jasmine unit test framework for the test suite. I used PhantomJS as a headless browser, so all test can be seen directly on the command line. All unit test are found directly in the /test folder
To run test there is a Grunt task or there is a pre configured NPM task command called here:

```sh
$ grunt test
$ npm test
```

###End To End Testing (Protractor)
for end to end front-end testing I am using Protractor. Using NPM, it will spin up a live browser and do a full automation test. All end to end test are found in the /test/e2e folder

Protractor simulates interaction with our web app and verifies that the application responds correctly. Therefore, our web server needs to be serving up the application, so that Protractor can interact with it:

```sh
$ npm start
```

In addition, since Protractor is built upon WebDriver we need to install this:
```sh
$ npm run update-webdriver
```

Once you have ensured that the development web server hosting our application is up and running and WebDriver is updated, you can run the end-to-end tests using the supplied npm script:
```sh
$ npm run protractor
```

If you have some issues using NPM to run protractor test, you can run it directly using protractor:
```sh
$ protractor protractor.conf.js 
```

#Trouble Shooting

### SASS / CSS
[SASS] is the CSS preprocessor we use on the front-end to write all our CSS. No frame work was used as part of this test.

**Installation**
```sh
$ sudo gem install sass
```

### Grunt
[Grunt] is used as the task automation tool for the front-end. The main usages is to compile SASS to its appropriate CSS. It can also be used as part of development to run Jasmine test.
All configuration can be found in the Gruntfile.js.

**Installation**
```sh
$ npm install -g grunt-cli
$ npm install
```

### Bower
[Bower] is used to manage all front-end dependencies/libraries. All required packages are defined in the bower.json file. Developers working on the application should always run a 'bower install' or 'bower update' to ensure they have all the latest packages.

**Installation**
```sh
$ npm install -g bower
$ bower install
```

### AngularJS
[AngularJS] is the MVC frame work used as part of this project.

## Suggested Improvements
- Use Angular Protractor to produce a proper End-To-End test suite.
- Use Karma to run test in browser.
- Use RequireJS/AngularAMD for modular javascript library loading.
- clean up the NPM modules.
- Use BEM for CSS naming conventions.

**Resources**

[Bootstrap]:http://getbootstrap.com/
[Grunt]:http://gruntjs.com/getting-started
[Getting Started With Grunt and SASS]:http://ryanchristiani.com/getting-started-with-grunt-and-sass/
[SASS]:http://sass-lang.com/
[Bower]:http://bower.io/
[NodeJS]:https://nodejs.org/
[RequireJS]:http://requirejs.org/
[AngularJS]:https://angularjs.org/