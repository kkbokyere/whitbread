/**
 * @function foursquareService
 * @memberOf whitbreadApp
 * @description this service provides the interface for the foursquare api. any new methods and API based logic can be added here. As a singleton, the keyword this refers to the object instance.
 */
angular.module('whitbreadApp')
    .service('foursquareService', ['$http', function ($http) {

        this.apiEndPoint = 'https://api.foursquare.com/v2/venues/search';
        this.clientId = 'BK45Y55G0YXYKCE5OENP55FOV01DOTS5OWGH2VFLM4LXYHDT';
        this.clientSecret = '2NTAQQL31CPUONT3J3H4YEF40EO344RIL2PWVRMBKCXE3K5Z';
        this.versionDate ='20140806';
        this.m ='foursquare';
        this.near ='London, UK';

        this.endpoint = this.apiEndPoint+'?v='+this.versionDate+'&client_id='+this.clientId+'&client_secret='+this.clientSecret+'&near='+this.near+'&query=';

        //https://api.foursquare.com/v2/venues/search GET
        this.searchForVenue = function (queryString) {
            return $http.get(this.endpoint+queryString);
        };

    }]);