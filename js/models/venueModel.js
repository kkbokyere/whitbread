/**
 * @function venueModel
 * @memberOf whitbreadApp
 * @description this calls the foursquare api and collates the data.
 */
angular.module('whitbreadApp')
    .service('venueModel', function () {
        function venue(id,name, fullAddress, postcode, city, contactNumber, website, category, icon) {
            this.id = id;
            this.name = name;
            this.fulladdress = fullAddress;
            this.postcode = postcode;
            this.city = city;
            this.contactNumber = contactNumber;
            this.website = website;
            this.category = category;
        }

        return {
            formatVenueDetails: function (data) {
                var venuesList = [];
                data.response.venues.forEach(function(venueResponse){
                    venuesList.push(new venue(
                        venueResponse.id,
                        venueResponse.name,
                        venueResponse.location.formattedAddress,
                        venueResponse.location.postalCode,
                        venueResponse.location.city,
                        venueResponse.contact.formattedPhone,
                        venueResponse.url,
                        venueResponse.categories.name
                    ));
                });
                return venuesList;
            },
            getVenues: function () {
                return venues
            }
        };
    });