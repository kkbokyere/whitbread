/**
 * @function whitbreadCtrl
 * @memberOf whitbreadApp
 * @description the main controller for the
 */
angular.module('whitbreadApp', ['ngRoute'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/searchForm.html',
                controller: 'whitbreadCtrl'
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })
    .controller('whitbreadCtrl', function ($scope, venueModel, foursquareService) {
        $scope.$watch('searchText', function(val){
            $scope.searchForVenue(val);
        });
        $scope.searchForVenue = function(searchQuery) {
            $scope.venues = '';
            if (searchQuery) {
                foursquareService.searchForVenue(searchQuery)
                    .success(function(response) {
                        $scope.venues = venueModel.formatVenueDetails(response);
                    });
            } else {
                $scope.venues = ''
            }
        };
    });
